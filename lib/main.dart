import 'package:flutter/material.dart';
import 'package:pos_on_man/presentation/components/theme_constants.dart';
import 'package:pos_on_man/presentation/routes/app_router.dart';
import 'package:pos_on_man/presentation/foundation/app_localizations.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  MyApp({super.key});
  final appRouter = AppRouter();
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
      debugShowCheckedModeBanner: false,
      onGenerateTitle: (BuildContext context) {
        return AppLocalizations.of(context)?.appTitle ?? "";
      },
      localizationsDelegates: AppLocalizations.localizationsDelegates,
      supportedLocales: AppLocalizations.supportedLocales,
      theme: ThemeData(
        primaryColor: kPrimaryColor,
        scaffoldBackgroundColor: Colors.white
      ),
      routerConfig: appRouter.config(),
    );
  }
}