import 'package:flutter/material.dart';
import 'package:pos_on_man/presentation/components/kv_elevated_button.dart';
import 'package:pos_on_man/presentation/components/kv_outlined_button.dart';
import 'package:pos_on_man/presentation/components/vertical_spacing.dart';
import 'package:pos_on_man/presentation/foundation/assets.gen.dart';
import 'package:pos_on_man/presentation/foundation/app_localizations.dart';

class Body extends StatelessWidget {
const Body({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context){
    Size size = MediaQuery.of(context).size;
    return SizedBox(
      height: size.height, 
      width: size.width,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          KVAssets.images.kiotvietLogo2022.image(width: size.width * 0.3),
          const VerticalSpacing(spacing: 48),
          KVAssets.images.onboarding.image(width: size.width * 0.7),
          const VerticalSpacing(spacing: 24),
          const Text(
            "Phổ biến nhất",
            style: TextStyle(
              fontWeight: FontWeight.bold, 
              color: Colors.black
            ),
          ),
          const VerticalSpacing(spacing: 16),
          SizedBox(
            width: size.width * 0.7,
            child: const Text(
              "Đơn giản, dễ dùng, tiết kiệm chi phí, phù hợp tất cả ngành hàng",
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Colors.black
              )
            ),
          ),
          const VerticalSpacing(spacing: 60),
          const KVElevatedButton(title: "Đăng ký"),
          const VerticalSpacing(spacing: 16),
          KVOutlinedButton(title: AppLocalizations.of(context)!.login)
        ],
      )
    );
  }
}



