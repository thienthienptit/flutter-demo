part of 'wellcome_bloc.dart';

@freezed
abstract class WellcomeState with _$WellcomeState {
  const factory WellcomeState.initial() = _Initial;
}
