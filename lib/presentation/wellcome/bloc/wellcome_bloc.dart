// ignore: depend_on_referenced_packages
import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'wellcome_event.dart';
part 'wellcome_state.dart';
part 'wellcome_bloc.freezed.dart';

class WellcomeBloc extends Bloc<WellcomeEvent, WellcomeState> {
  WellcomeBloc() : super(const _Initial()) {
    on<WellcomeEvent>((event, emit) {
      // TODO: implement event handler
    });
  }
}
