part of 'wellcome_bloc.dart';

@freezed
abstract class WellcomeEvent with _$WellcomeEvent {
  const factory WellcomeEvent.started() = _Started;
  const factory WellcomeEvent.signingIn() = _SigningIn;
  const factory WellcomeEvent.signingUp() = _SigningUp;
}