import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:pos_on_man/presentation/wellcome/components/body.dart';

@RoutePage()
class WellcomeScreen extends StatelessWidget {
const WellcomeScreen({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context){
    return const Scaffold(body: Body());
  }
}