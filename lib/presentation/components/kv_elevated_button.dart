import 'package:flutter/material.dart';
import 'package:pos_on_man/presentation/components/theme_constants.dart';


class KVElevatedButton extends StatelessWidget {
  const KVElevatedButton({
    super.key, 
    required this.title, 
    this.backgroundColor = kPrimaryColor, 
    this.textColor = Colors.white, 
    this.width, 
    this.height,
    this.radius
  });

  final String title;
  final Color backgroundColor, textColor;
  final double? width, height, radius;

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return ElevatedButton(
        onPressed: () {},
        style: ElevatedButton.styleFrom(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(radius ?? 8)
          ),
          backgroundColor: backgroundColor,
          minimumSize: Size(width ?? size.width * 0.8, height ?? 44)
        ),
        child: Text(
            title, 
            style: TextStyle(color: textColor),
        )
    );
  }
}