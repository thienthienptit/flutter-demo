import 'package:flutter/material.dart';

class VerticalSpacing extends StatelessWidget {
  const VerticalSpacing({
    super.key,
    this.spacing = 1
  });
  final double spacing;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: spacing
    );
  }
}