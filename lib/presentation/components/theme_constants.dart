import 'package:flutter/material.dart';

const kPrimaryColor = Color(0xFF237FCD);
const kSecondaryColor = Color(0xFF4CB050);