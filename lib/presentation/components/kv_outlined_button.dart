import 'package:flutter/material.dart';
import 'package:pos_on_man/presentation/components/theme_constants.dart';

class KVOutlinedButton extends StatelessWidget {
  const KVOutlinedButton({
    super.key,
    required this.title, 
    this.borderColor = kPrimaryColor, 
    this.textColor = kPrimaryColor, 
    this.width, 
    this.height, 
    this.radius,
  });

  final String title;
  final Color borderColor, textColor;
  final double? width, height, radius;

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return OutlinedButton(
      onPressed: () {},
      style: OutlinedButton.styleFrom(
        side: BorderSide(width: radius ?? 1, color: borderColor),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(8)
        ),
        minimumSize: Size(width ?? size.width * 0.8, height ?? 44)
      ), 
      child: Text(
        title,
        style: TextStyle(color: textColor),
      ));
  }
}